webpackJsonp([10],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CdBasicosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_geocoder__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_location_accuracy__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_services_demo_service__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_services_task_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__tabs_tabs__ = __webpack_require__(52);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var CdBasicosPage = (function () {
    function CdBasicosPage(navCtrl, viewCtrl, navParams, alertCtrl, loadingCtrl, fb, geolocation, nativeGeocoder, toastCtrl, network, locationAccuracy, demoService, tasksService) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.fb = fb;
        this.geolocation = geolocation;
        this.nativeGeocoder = nativeGeocoder;
        this.toastCtrl = toastCtrl;
        this.network = network;
        this.locationAccuracy = locationAccuracy;
        this.demoService = demoService;
        this.tasksService = tasksService;
        this.rForm = fb.group({
            'nombre': [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            'apellidos': [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            'correo': [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            'fnacimiento': [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            'celular': [null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            'validate': ''
        });
        /*  this.geolocation.getCurrentPosition().then((resp) => {
       // resp.coords.latitude
       // resp.coords.longitude
       console.log(resp.coords.latitude + ' ' + resp.coords.longitude);
      
       this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
       .then((result: NativeGeocoderReverseResult) => {
      
         this.position = JSON.stringify(result);
      
         let alert12 = this.alertCtrl.create({
         title: 'Usted esta en:',
         subTitle: this.position,
         buttons: ['OK']
       });
      
       alert12.present();
         console.log(JSON.stringify(result));
      
       }).catch((error: any) => {
      
         let alert11 = this.alertCtrl.create({
         title: 'Usted esta en:',
         subTitle: error,
         buttons: ['OK']
         });
      
         alert11.present();
       });
      
      }).catch((error) => {
        console.log('Error getting location', error);
      });*/
        /*Validar cuando no este disponible la conexion del dispositivo
        let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        
          this.vInternet=false;
        
        });
        //Validar cuando este disponible la conexion del dispositivo
        let connectSubscription = this.network.onConnect().subscribe(() => {
        
        setTimeout(() => {
        
          this.vInternet=true;
        
        }, 3000);
        
        });*/
    }
    //---------------------------------------
    CdBasicosPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    //---------------------------------------
    CdBasicosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CdBasicosPage');
    };
    //---------------------------------------
    CdBasicosPage.prototype.insert = function (params) {
        var _this = this;
        /*this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    
      if(canRequest) {
        // the accuracy option will be ignored by iOS
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
          () => {*/
        var loader = this.loadingCtrl.create({
            content: "Validando Datos..."
        });
        loader.present();
        this.demoService.prueba().subscribe(function (response) {
            var alertss = _this.alertCtrl.create({
                title: 'Cosumo existoso',
                subTitle: 'Se han insertado los datos correctamente',
                buttons: ['OK']
            });
            loader.dismiss();
            alertss.present();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__tabs_tabs__["a" /* TabsPage */]);
        }, function (error) {
            var alerterr = _this.alertCtrl.create({
                title: 'No hay conexion',
                subTitle: 'Se esperara conexion para poder insertar el registro, registro guardado en la base de datos local',
                buttons: ['OK']
            });
            var alerterr2 = _this.alertCtrl.create({
                title: 'No hay conexion',
                subTitle: 'Se esperara conexion para poder insertar el registro, registro no guardado',
                buttons: ['OK']
            });
            _this.tasksService.create(params)
                .then(function (response) {
                loader.dismiss();
                alerterr.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__tabs_tabs__["a" /* TabsPage */]);
            })
                .catch(function (error) {
                loader.dismiss();
                alerterr2.present();
            });
        });
        /*},
        error => {
          let alertGPS = this.alertCtrl.create({
               title: 'Error al reclutar',
               message: 'No se puede reclutar hasta que se encienda el gps de tu dispositivo',
               buttons: ['OK']
               });

       alertGPS.present();
    });
  }
});*/
        /*console.log(params);
      let loader = this.loadingCtrl.create({
          content: "Validando Datos...",
          duration: 3000
        });
        loader.present();
        setTimeout(() => {
          let alert = this.alertCtrl.create({
          title: 'Registro exitoso!',
          subTitle: 'Se han guardado los datos del reclutado con exito!',
          buttons: ['OK']
        });
        alert.present(this.viewCtrl.dismiss());
      },3000);*/
    };
    return CdBasicosPage;
}());
CdBasicosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-cd-basicos',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/cd-basicos/cd-basicos.html"*/'<!--\n  Generated template for the CdBasicosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-toolbar>\n\n      <ion-title text-center>\n        Reclutamiento\n      </ion-title>\n\n      <ion-buttons start>\n\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="primary" showWhen="ios" style="color:white;">Cancel</span>\n          <ion-icon name="md-close" showWhen="android,windows" style="color:white;"></ion-icon>\n        </button>\n\n      </ion-buttons>\n\n    </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <form (ngSubmit)="insert(rForm.value);" [formGroup]="rForm">\n      <ion-list>\n        <ion-item>\n          <ion-icon name="person" item-left></ion-icon>\n          <ion-label stacked>Nombres:</ion-label>\n          <ion-input type="text" placeholder="Nombre" formControlName="nombre"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="person" item-left></ion-icon>\n          <ion-label stacked>Apellidos:</ion-label>\n          <ion-input type="text" placeholder="Apellidos" formControlName="apellidos"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="mail" item-left></ion-icon>\n          <ion-label stacked>Correo electronico:</ion-label>\n          <ion-input type="email" placeholder="Email" formControlName="correo"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="calendar" item-left></ion-icon>\n          <ion-label stacked>Fecha de nacimiento:</ion-label>\n          <ion-datetime displayFormat="DD-MM-YYYY" formControlName="fnacimiento" placeholder="MM-DD-AAA"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="call" item-left></ion-icon>\n          <ion-label stacked>Celular:</ion-label>\n          <ion-input type="tel" maxlength="10" formControlName="celular" placeholder="XXX-XXX-XXXX"></ion-input>\n        </ion-item>\n      </ion-list>\n      <div padding>\n        <button ion-button block type="submit" [disabled]="!rForm.valid">Guardar</button>\n      </div>\n    </form>\n\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/cd-basicos/cd-basicos.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_7__app_services_demo_service__["a" /* DemoService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_location_accuracy__["a" /* LocationAccuracy */], __WEBPACK_IMPORTED_MODULE_7__app_services_demo_service__["a" /* DemoService */], __WEBPACK_IMPORTED_MODULE_8__app_services_task_service__["a" /* TaskService */]])
], CdBasicosPage);

//# sourceMappingURL=cd-basicos.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CdDocumentosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CdDocumentosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CdDocumentosPage = (function () {
    function CdDocumentosPage(navCtrl, viewCtrl, navParams, alertCtrl, loadingCtrl, camera, fb) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.fb = fb;
        this.show1 = false;
        this.show2 = false;
        this.show3 = false;
        this.rForm = fb.group({
            'nombre': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'apellidos': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'correo': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'fecha': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'celular': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'validate': ''
        });
    }
    CdDocumentosPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CdDocumentosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CdDocumentosPage');
    };
    CdDocumentosPage.prototype.getPicture = function () {
        var _this = this;
        var options = {
            targetWidth: 800,
            targetHeight: 800,
            quality: 100,
            allowEdit: true,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.show1 = true;
            _this.image = "data:image/jpeg;base64," + imageData;
            var galleryImageSelector = document.getElementById('acta');
            galleryImageSelector.setAttribute('src', _this.image);
            /*let alert = this.alertCtrl.create({
              title: 'Fallo',
            subTitle: this.image,
            buttons: ['OK']});
            alert.present();*/
        }).catch(function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Fallo',
                subTitle: error,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    CdDocumentosPage.prototype.getPicture2 = function () {
        var _this = this;
        var options2 = {
            targetWidth: 800,
            targetHeight: 800,
            quality: 100,
            allowEdit: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options2).then(function (imageData2) {
            _this.show2 = true;
            _this.image2 = "data:image/jpeg;base64," + imageData2;
            var galleryImageSelector2 = document.getElementById('domicilio');
            galleryImageSelector2.setAttribute('src', _this.image2);
            /*let alert = this.alertCtrl.create({
              title: 'Fallo',
            subTitle: this.image2,
            buttons: ['OK']});
            alert.present();*/
        }).catch(function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Fallo',
                subTitle: error,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    CdDocumentosPage.prototype.getPicture3 = function () {
        var _this = this;
        var options3 = {
            targetWidth: 800,
            targetHeight: 800,
            quality: 100,
            allowEdit: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options3).then(function (imageData3) {
            _this.show3 = true;
            _this.image3 = "data:image/jpeg;base64," + imageData3;
            var galleryImageSelector3 = document.getElementById('certificado');
            galleryImageSelector3.setAttribute('src', _this.image3);
            /*let alert = this.alertCtrl.create({
              title: 'Fallo',
            subTitle: this.image,
            buttons: ['OK']});
            alert.present();*/
        }).catch(function (error) {
            var alert = _this.alertCtrl.create({
                title: 'Fallo',
                subTitle: error,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    CdDocumentosPage.prototype.insert = function (params) {
        var _this = this;
        if (this.image == null) {
            var modal1 = this.alertCtrl.create({
                title: 'Faltan Archivos!',
                subTitle: 'Tomar foto del documento acta!',
                buttons: ['OK']
            });
            modal1.present();
        }
        else if (this.image2 == null) {
            var modal2 = this.alertCtrl.create({
                title: 'Faltan Archivos!',
                subTitle: 'Tomar foto del documento domicilio!',
                buttons: ['OK']
            });
            modal2.present();
        }
        else if (this.image3 == null) {
            var modal3 = this.alertCtrl.create({
                title: 'Faltan Archivos!',
                subTitle: 'Tomar foto del documento certificado!',
                buttons: ['OK']
            });
            modal3.present();
        }
        else {
            var loader = this.loadingCtrl.create({
                content: "Validando Datos...",
                duration: 3000
            });
            loader.present();
            setTimeout(function () {
                var alert = _this.alertCtrl.create({
                    title: 'Registro exitoso!',
                    subTitle: 'Se han guardado los datos del reclutado con exito!',
                    buttons: ['OK']
                });
                alert.present(_this.viewCtrl.dismiss());
            }, 3000);
        }
    };
    return CdDocumentosPage;
}());
CdDocumentosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-cd-documentos',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/cd-documentos/cd-documentos.html"*/'<!--\n  Generated template for the CdDocumentosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-toolbar>\n\n      <ion-title text-center>\n        Reclutamiento\n      </ion-title>\n\n      <ion-buttons start>\n\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="primary" showWhen="ios" style="color:white;">Cancel</span>\n          <ion-icon name="md-close" showWhen="android,windows" style="color:white;"></ion-icon>\n        </button>\n\n      </ion-buttons>\n\n    </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n  <form (ngSubmit)="insert(rForm.value)" [formGroup]="rForm">\n      <ion-list>\n        <ion-item>\n          <ion-icon name="person" item-left></ion-icon>\n          <ion-label stacked>Nombres:</ion-label>\n          <ion-input type="text" placeholder="Nombre" formControlName="nombre"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="person" item-left></ion-icon>\n          <ion-label stacked>Apellidos:</ion-label>\n          <ion-input type="text" placeholder="Apellidos" formControlName="apellidos"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="mail" item-left></ion-icon>\n          <ion-label stacked>Correo electronico:</ion-label>\n          <ion-input type="email" placeholder="Email" formControlName="correo"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="calendar" item-left></ion-icon>\n          <ion-label stacked>Fecha de nacimiento:</ion-label>\n          <ion-datetime displayFormat="MM-DD-YYYY" placeholder="MM-DD-AAAA" formControlName="fecha"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-icon name="call" item-left></ion-icon>\n          <ion-label stacked>Celular:</ion-label>\n          <ion-input type="tel" maxlength="10" placeholder="XXX-XXX-XXXX" formControlName="celular"></ion-input>\n        </ion-item>\n      </ion-list>\n\n      <ion-row justify-content-center>\n        <ion-label>Acta de Nacimiento: </ion-label><button ion-button small  type="button" color="secondary" (click)="getPicture()"><ion-icon name="camera"></ion-icon></button>\n      </ion-row>\n      <img id="acta" [hidden]="!show1"/>\n\n      <ion-row justify-content-center>\n        <ion-label>Comprobante de domicilio: </ion-label><button ion-button small  type="button" color="secondary" (click)="getPicture2()"><ion-icon name="camera"></ion-icon></button>\n      </ion-row>\n      <img id="domicilio" [hidden]="!show2"/>\n\n      <ion-row justify-content-center>\n        <ion-label>Certificado de estudios: </ion-label><button ion-button small  type="button" color="secondary" (click)="getPicture3()"><ion-icon name="camera"></ion-icon></button>\n      </ion-row>\n      <img id="certificado" [hidden]="!show3"/>\n\n      <div padding>\n        <button ion-button block type="submit" [disabled]="!rForm.valid">Guardar</button>\n      </div>\n\n    </form>\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/cd-documentos/cd-documentos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], CdDocumentosPage);

//# sourceMappingURL=cd-documentos.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuOpcionesPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the MenuOpcionesPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MenuOpcionesPopoverPage = (function () {
    function MenuOpcionesPopoverPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    MenuOpcionesPopoverPage.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad MenuOpcionesPopoverPage');
    };
    MenuOpcionesPopoverPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    MenuOpcionesPopoverPage.prototype.cerrarSesion = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    return MenuOpcionesPopoverPage;
}());
MenuOpcionesPopoverPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-menu-opciones-popover',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/menu-opciones-popover/menu-opciones-popover.html"*/'<ion-list>\n  <br>\n  <button ion-button full icon-left clear (click)="cerrarSesion()">\n    <ion-icon name="exit"></ion-icon>\n    Cerrar Sesion\n  </button>\n</ion-list>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/menu-opciones-popover/menu-opciones-popover.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], MenuOpcionesPopoverPage);

//# sourceMappingURL=menu-opciones-popover.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.changeImgProfile = function () {
        var _this = this;
        var options = {
            targetWidth: 1000,
            targetHeight: 1000,
            quality: 100,
            allowEdit: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 1,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options).then(function (imageData) {
            _this.image = "data:image/jpeg;base64," + imageData;
            var galleryImageSelector = document.getElementById('avatar');
            galleryImageSelector.setAttribute('src', _this.image);
            /*let alert = this.alertCtrl.create({
              title: 'Fallo',
            subTitle: this.image,
            buttons: ['OK']});
            alert.present();*/
        }).catch(function (error) {
            console.log(error);
        });
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/profile/profile.html"*/'<!--\n  Generated template for the ProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center>Perfil</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-card style="background: linear-gradient(#1e4873, #1d4d80, #1d4d80">\n\n        <ion-grid>\n          <ion-row justify-content-center>\n            <ion-item>\n          <ion-avatar>\n            <img style="width: 150px; height: 150px;" src="assets/img/user1.png" alt="Imagen Usuario" id="avatar"/>\n          </ion-avatar>\n        </ion-item>\n      </ion-row>\n\n            <ion-row justify-content-center>\n                <ion-col col-6>\n                    <button ion-button icon-left block color="primary" small (click)="changeImgProfile()">\n                    <ion-icon name="camera"></ion-icon>\n                    Cambiar Foto\n                    </button>\n                </ion-col>\n            </ion-row>\n\n        <ion-card-content>\n            <ion-card-title>\n              <h1 text-center>Demo</h1>\n            </ion-card-title>\n        </ion-card-content>\n        <hr>\n        <ion-item style="background: none;">\n                <h2 text-center>Demo Demo Demo</h2>\n            </ion-item>\n            <ion-item style="background: none;">\n                <h2 text-center>demo@demo.com</h2>\n            </ion-item>\n            <ion-item style="background: none;">\n                <h2 text-center>Col. Demo calle Demo #000</h2>\n            </ion-item>\n\n            <ion-row justify-content-center>\n\n            <button ion-button outline color="danger">Cambiar Contraseña</button>\n\n        </ion-row>\n\n        </ion-grid>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/profile/profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MetasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the MetasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MetasPage = (function () {
    function MetasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.metas = [
            {
                descripcion: "Recluta un total de 10 personas el dia de hoy",
                rangoMeta: 10,
                rangoMetaActual: 2,
            },
            {
                descripcion: "Recluta un total de 100 personas en este mes",
                rangoMeta: 100,
                rangoMetaActual: 2
            },
            {
                descripcion: "Recluta 5 personas con sus documentos",
                rangoMeta: 5,
                rangoMetaActual: 3
            },
            {
                descripcion: "Recluta un total de 4 personas",
                rangoMeta: 4,
                rangoMetaActual: 4
            },
            {
                descripcion: "Recluta 2 personas con sus documentos",
                rangoMeta: 2,
                rangoMetaActual: 2
            }
        ];
    }
    MetasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MetasPage');
    };
    return MetasPage;
}());
MetasPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-metas',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/metas/metas.html"*/'<!--\n  Generated template for the MetasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title text-center>Metas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n  <ion-row>\n    <ion-title><h2>Metas disponibles:</h2></ion-title>\n  </ion-row>\n<!--  - - - - - - - - - - - - - - - - - - -->\n  <ion-card>\n    <ion-card-header>\n      <h2 text-center style="color: #a2c30d;">Metas Pendientes</h2>\n    </ion-card-header>\n  <div *ngFor="let meta of metas">\n<ion-row *ngIf="meta.rangoMetaActual < meta.rangoMeta">\n    <ion-col col-6 style="border-top: 2px solid #abb0b4;color:white">{{meta.descripcion}}</ion-col>\n    <ion-col col-6 style="border-top: 2px solid #abb0b4;color:white">\n      <progress id="progressbar" max="{{meta.rangoMeta}}" value="{{meta.rangoMetaActual}}"> </progress>\n      <div id="progressbarlabel" text-center>{{meta.rangoMetaActual}}/{{meta.rangoMeta}} </div>\n    </ion-col>\n</ion-row>\n  </div>\n</ion-card>\n\n<!--  - - - - - - - - - - - - - - - - - - -->\n<br>\n<ion-card>\n  <ion-card-header>\n    <h2 text-center style="color: #d1163b;">Metas Terminadas</h2>\n  </ion-card-header>\n\n<div *ngFor="let meta of metas">\n<ion-row *ngIf="meta.rangoMetaActual >= meta.rangoMeta">\n  <ion-col col-6 style="border-top: 2px solid #abb0b4;color:white">{{meta.descripcion}}</ion-col>\n  <ion-col col-6 style="border-top: 2px solid #abb0b4;color:white">\n    <progress id="progressbar" max="1" value="{{ 1 }}"> </progress>\n    <div id="progressbarlabel" text-center><strong>Finalizada </strong></div>\n  </ion-col>\n</ion-row>\n\n</div>\n</ion-card>\n\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/metas/metas.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], MetasPage);

//# sourceMappingURL=metas.js.map

/***/ }),

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sent_sent__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pending_pending__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_services_task_service__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the HistorialPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var HistorialPage = (function () {
    function HistorialPage(navCtrl, navParams, modalCtrl, tasksService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.tasksService = tasksService;
        this.tasksSent = [];
        this.tasksPending = [];
    }
    HistorialPage.prototype.ionViewDidLoad = function () {
        this.getAllTasksSent();
        this.getAllTasksPending();
    };
    HistorialPage.prototype.Enviados = function (obj) {
        var enviadosModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__sent_sent__["a" /* SentPage */], { obj: obj });
        enviadosModal.present();
    };
    HistorialPage.prototype.Pendientes = function (obj) {
        var pendientesModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pending_pending__["a" /* PendingPage */], { obj: obj });
        pendientesModal.present();
    };
    HistorialPage.prototype.getAllTasksPending = function () {
        var _this = this;
        this.tasksService.getAllPending()
            .then(function (tasks) {
            console.log(tasks);
            _this.tasksPending = tasks;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    HistorialPage.prototype.getAllTasksSent = function () {
        var _this = this;
        this.tasksService.getAllSent()
            .then(function (tasks) {
            console.log(tasks);
            _this.tasksSent = tasks;
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    return HistorialPage;
}());
HistorialPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-historial',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/historial/historial.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title text-center>Historial</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="list-avatar-page" padding>\n\n  <ion-list>\n\n    <ion-list-header>\n      <h1 text-center style="color: #a2c30d;">ENVIADOS</h1>\n    </ion-list-header>\n\n    <div *ngIf="tasksSent.length==0">\n      <h4 text-center><strong>Ningun Registro Enviado</strong></h4>\n    </div>\n\n    <ion-item *ngFor="let task of tasksSent; let i = index">\n      <h2> {{ task.nombres + \' \' + task.apellidos }}</h2>\n      <p>{{ task.correo }}</p>\n      <ion-note item-end>{{ task.fnacimiento }}</ion-note>\n      <button item-end ion-button color="primary-two" (click)="Enviados(task)">Ver</button>\n    </ion-item>\n  </ion-list>\n\n   <br>\n\n  <ion-list>\n\n    <ion-list-header>\n      <h1 text-center style="color: #d1163b;">PENDIENTES</h1>\n    </ion-list-header>\n\n    <div *ngIf="tasksPending.length==0">\n      <h4 text-center><strong>Ningun Registro Pendiente</strong></h4>\n    </div>\n\n    <ion-item *ngFor="let task of tasksPending; let i = index">\n\n      <h2> {{ task.nombres + \' \' + task.apellidos }}</h2>\n      <p>{{ task.correo }}</p>\n      <ion-note item-end>{{ task.fnacimiento }}</ion-note>\n\n      <button item-end ion-button color="primary-two" (click)="Pendientes(task)">Ver</button>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/historial/historial.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4__app_services_task_service__["a" /* TaskService */]])
], HistorialPage);

//# sourceMappingURL=historial.js.map

/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SentPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SentPage = (function () {
    function SentPage(navCtrl, navParams, viewCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.obj = this.navParams.get("obj");
        console.log(this.obj);
    }
    SentPage.prototype.cancelModal = function () {
        this.viewCtrl.dismiss();
    };
    return SentPage;
}());
SentPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-sent',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/sent/sent.html"*/'<!--\n  Generated template for the SentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n\n</ion-header>\n\n\n<ion-content padding>\n<button ion-button clear (click)="cancelModal()">Cancelar</button>\n\n<br><br>\n  <ion-item>\n      <ion-icon name=\'contact\' item-start></ion-icon>\n      Nombre: <strong>{{obj.nombres}}</strong>\n   </ion-item>\n   <ion-item>\n       <ion-icon name=\'contact\' item-start></ion-icon>\n       Lugar: <strong>{{obj.apellidos}}</strong>\n    </ion-item>\n    <ion-item>\n        <ion-icon name=\'at\' item-start></ion-icon>\n        Fecha De Nacimiento: <strong>{{obj.fnacimiento}}</strong>\n     </ion-item>\n     <ion-item>\n         <ion-icon name=\'time\' item-start></ion-icon>\n         Correo: <strong>{{obj.correo}}</strong>\n      </ion-item>\n      <ion-item>\n          <ion-icon name=\'checkmark-circle\' item-start></ion-icon>\n          Status: <strong>Pendiente</strong>\n       </ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/sent/sent.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], SentPage);

//# sourceMappingURL=sent.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PendingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PendingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PendingPage = (function () {
    function PendingPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.obj = this.navParams.get("obj");
        console.log(this.obj);
    }
    PendingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PendingPage');
    };
    PendingPage.prototype.cancelModal = function () {
        this.viewCtrl.dismiss();
    };
    return PendingPage;
}());
PendingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-pending',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/pending/pending.html"*/'<!--\n  Generated template for the PendingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n\n</ion-header>\n\n\n<ion-content padding>\n  <button ion-button clear (click)="cancelModal()">Cancelar</button>\n\n  <br><br>\n    <ion-item>\n        <ion-icon name=\'contact\' item-start></ion-icon>\n        Nombre: <strong>{{obj.nombres}}</strong>\n     </ion-item>\n     <ion-item>\n         <ion-icon name=\'contact\' item-start></ion-icon>\n         Lugar: <strong>{{obj.apellidos}}</strong>\n      </ion-item>\n      <ion-item>\n          <ion-icon name=\'at\' item-start></ion-icon>\n          Fecha De Nacimiento: <strong>{{obj.fnacimiento}}</strong>\n       </ion-item>\n       <ion-item>\n           <ion-icon name=\'time\' item-start></ion-icon>\n           Correo: <strong>{{obj.correo}}</strong>\n        </ion-item>\n        <ion-item>\n            <ion-icon name=\'checkmark-circle-outline\' item-start></ion-icon>\n            Status: <strong>Pendiente</strong>\n         </ion-item>\n\n\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/pending/pending.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], PendingPage);

//# sourceMappingURL=pending.js.map

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 124;

/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cd-basicos/cd-basicos.module": [
		283,
		9
	],
	"../pages/cd-documentos/cd-documentos.module": [
		284,
		8
	],
	"../pages/historial/historial.module": [
		290,
		7
	],
	"../pages/login/login.module": [
		292,
		6
	],
	"../pages/menu-opciones-popover/menu-opciones-popover.module": [
		285,
		5
	],
	"../pages/metas/metas.module": [
		287,
		4
	],
	"../pages/pending/pending.module": [
		289,
		3
	],
	"../pages/profile/profile.module": [
		286,
		2
	],
	"../pages/sent/sent.module": [
		288,
		1
	],
	"../pages/tabs/tabs.module": [
		291,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 166;

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cd_basicos_cd_basicos__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cd_documentos_cd_documentos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__menu_opciones_popover_menu_opciones_popover__ = __webpack_require__(109);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = (function () {
    function HomePage(modalCtrl, navCtrl, popoverCtrl) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
    }
    HomePage.prototype.basicoModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__cd_basicos_cd_basicos__["a" /* CdBasicosPage */]);
        modal.present();
    };
    HomePage.prototype.documentosModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__cd_documentos_cd_documentos__["a" /* CdDocumentosPage */]);
        modal.present();
    };
    HomePage.prototype.slideChanged = function () {
        //this.slides.slideNext(500, true);
        //let currentIndex = this.slides.getActiveIndex();
        //console.log('Current index is', currentIndex);
    };
    HomePage.prototype.openOpciones = function (event) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__menu_opciones_popover_menu_opciones_popover__["a" /* MenuOpcionesPopoverPage */]);
        popover.present({ ev: event });
        //popover.onDidDismiss();
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>\n            Inicio\n        </ion-title>\n        <ion-buttons end>\n            <button ion-button icon-only (click)="openOpciones($event)">\n                <ion-icon name="more" color="light"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n    <ion-grid>\n        <img src="assets/img/logo.png" alt="imagen_inicio">\n        <br>\n        <h2 text-center style="color:white;">Opciones de Registro</h2>\n        <ion-row>\n            <ion-col col-12 col-sm-6>\n                <button ion-button full icon-left color="midnight" (click)="basicoModal()">\n                    <ion-icon name="document"></ion-icon>\n                    Captura Básica\n                </button>\n            </ion-col>\n            <ion-col col-12 col-sm-6>\n                <button ion-button full icon-left color="secondary-two" (click)="documentosModal()">\n                    <ion-icon name="folder"></ion-icon>\n                    Captura con documentos\n                </button>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n        <ion-slides spaceBetween="10" pager (ionSlideDidChange)="slideChanged()" autoplay="5000" loop=true effect="coverflow">\n            <ion-slide> <img src="assets/imagenes/imagen1.png"> </ion-slide>\n            <ion-slide> <img src="assets/imagenes/imagen1.png"> </ion-slide>\n            <ion-slide> <img src="assets/imagenes/imagen1.png"> </ion-slide>\n        </ion-slides>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(231);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_location_accuracy__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_metas_metas__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_historial_historial__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_cd_basicos_cd_basicos__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_cd_documentos_cd_documentos__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_menu_opciones_popover_menu_opciones_popover__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_pending_pending__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_sent_sent__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_geocoder_geocoder__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_sqlite__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_task_service__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_metas_metas__["a" /* MetasPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_historial_historial__["a" /* HistorialPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_cd_basicos_cd_basicos__["a" /* CdBasicosPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_cd_documentos_cd_documentos__["a" /* CdDocumentosPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_menu_opciones_popover_menu_opciones_popover__["a" /* MenuOpcionesPopoverPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_pending_pending__["a" /* PendingPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_sent_sent__["a" /* SentPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {}, {
                links: [
                    { loadChildren: '../pages/cd-basicos/cd-basicos.module#CdBasicosPageModule', name: 'CdBasicosPage', segment: 'cd-basicos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/cd-documentos/cd-documentos.module#CdDocumentosPageModule', name: 'CdDocumentosPage', segment: 'cd-documentos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/menu-opciones-popover/menu-opciones-popover.module#MenuOpcionesPopoverPageModule', name: 'MenuOpcionesPopoverPage', segment: 'menu-opciones-popover', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/metas/metas.module#MetasPageModule', name: 'MetasPage', segment: 'metas', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/sent/sent.module#SentPageModule', name: 'SentPage', segment: 'sent', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/pending/pending.module#PendingPageModule', name: 'PendingPage', segment: 'pending', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/historial/historial.module#HistorialPageModule', name: 'HistorialPage', segment: 'historial', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_6__angular_forms__["e" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_11__angular_http__["b" /* HttpModule */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_metas_metas__["a" /* MetasPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_historial_historial__["a" /* HistorialPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_cd_basicos_cd_basicos__["a" /* CdBasicosPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_cd_documentos_cd_documentos__["a" /* CdDocumentosPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_menu_opciones_popover_menu_opciones_popover__["a" /* MenuOpcionesPopoverPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_pending_pending__["a" /* PendingPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_sent_sent__["a" /* SentPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_network__["a" /* Network */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_24__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_location_accuracy__["a" /* LocationAccuracy */],
            __WEBPACK_IMPORTED_MODULE_25__ionic_native_sqlite__["a" /* SQLite */],
            __WEBPACK_IMPORTED_MODULE_26__services_task_service__["a" /* TaskService */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 263:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DemoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DemoService = (function () {
    function DemoService(_http) {
        this._http = _http;
        this.url = 'http://quejasydenuncias.tamaulipas.gob.mx';
    }
    DemoService.prototype.prueba = function () {
        return this._http.get(this.url + '/api/quejas').map(function (res) { return res.json(); });
    };
    return DemoService;
}());
DemoService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], DemoService);

//# sourceMappingURL=demo.service.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_task_service__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { HistorialPage } from '../pages/historial/historial';
//import { MetasPage } from '../pages/metas/metas';


var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, tasksService, sqlite) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.tasksService = tasksService;
        this.sqlite = sqlite;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.createDatabase();
        });
    }
    MyApp.prototype.createDatabase = function () {
        var _this = this;
        this.sqlite.create({
            name: 'data.db',
            location: 'default' // the location field is required
        })
            .then(function (db) {
            _this.tasksService.setDatabase(db);
            return _this.tasksService.createTable();
        })
            .then(function () {
            _this.splashScreen.hide();
        })
            .catch(function (error) {
            console.error(error);
        });
    };
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_6__services_task_service__["a" /* TaskService */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_sqlite__["a" /* SQLite */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the GeocoderProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var GeocoderProvider = (function () {
    function GeocoderProvider(http) {
        this.http = http;
        console.log('Hello GeocoderProvider Provider');
    }
    return GeocoderProvider;
}());
GeocoderProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], GeocoderProvider);

//# sourceMappingURL=geocoder.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TaskService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the TaskServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var TaskService = (function () {
    function TaskService(http) {
        this.http = http;
        this.db = null;
    }
    TaskService.prototype.setDatabase = function (db) {
        if (this.db === null) {
            this.db = db;
        }
    };
    TaskService.prototype.create = function (task) {
        var sql = 'INSERT INTO dbasicos(nombres, apellidos, correo, fnacimiento, status) VALUES(?,?,?,?,0)';
        return this.db.executeSql(sql, [task.nombre, task.apellidos, task.correo, task.fnacimiento]);
    };
    TaskService.prototype.createTable = function () {
        var sql = 'CREATE TABLE IF NOT EXISTS dbasicos(id INTEGER PRIMARY KEY AUTOINCREMENT, nombres TEXT, apellidos TEXT, correo TEXT, fnacimiento TEXT,status INTEGER)';
        return this.db.executeSql(sql, []);
    };
    TaskService.prototype.delete = function (task) {
        var sql = 'DELETE FROM dbasicos WHERE id=?';
        return this.db.executeSql(sql, [task.id]);
    };
    TaskService.prototype.getAllPending = function () {
        var sql = 'SELECT * FROM dbasicos WHERE status=0';
        return this.db.executeSql(sql, [])
            .then(function (response) {
            var tasks = [];
            for (var index = 0; index < response.rows.length; index++) {
                tasks.push(response.rows.item(index));
            }
            return Promise.resolve(tasks);
        })
            .catch(function (error) { return Promise.reject(error); });
    };
    TaskService.prototype.getAllSent = function () {
        var sql = 'SELECT * FROM dbasicos WHERE status=1';
        return this.db.executeSql(sql, [])
            .then(function (response) {
            var tasks = [];
            for (var index = 0; index < response.rows.length; index++) {
                tasks.push(response.rows.item(index));
            }
            return Promise.resolve(tasks);
        })
            .catch(function (error) { return Promise.reject(error); });
    };
    return TaskService;
}());
TaskService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
], TaskService);

//# sourceMappingURL=task-service.js.map

/***/ }),

/***/ 52:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__metas_metas__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__historial_historial__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the TabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var TabsPage = (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_5__historial_historial__["a" /* HistorialPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__metas_metas__["a" /* MetasPage */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabsPage');
    };
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-tabs',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/tabs/tabs.html"*/'<!--\n  Generated template for the TabsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-tabs>\n    <ion-tab tabIcon="home" [root]="tab1Root"></ion-tab>\n    <ion-tab tabIcon="list-box" [root]="tab2Root"></ion-tab>\n    <ion-tab tabIcon="star" [root]="tab4Root"></ion-tab>\n    <ion-tab tabIcon="person" [root]="tab3Root"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/tabs/tabs.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, loadingCtrl, fb, locationAccuracy, toastCtrl, network) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.fb = fb;
        this.locationAccuracy = locationAccuracy;
        this.toastCtrl = toastCtrl;
        this.network = network;
        this.correo = '';
        this.pass = '';
        this.vInternet = true;
        this.locationAccuracy.canRequest().then(function (canRequest) {
            if (canRequest) {
                // the accuracy option will be ignored by iOS
                _this.locationAccuracy.request(_this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(function () {
                    var suss = _this.toastCtrl.create({
                        message: 'Se ha activado la ubicacion',
                        duration: 3000,
                        position: 'bottom'
                    });
                    suss.onDidDismiss(function () {
                        console.log('Dismissed toast');
                    });
                    suss.present();
                }, function (error) {
                    var err = _this.toastCtrl.create({
                        message: error,
                        duration: 2700,
                        position: 'bottom'
                    });
                    err.onDidDismiss(function () {
                        console.log('Dismissed toast');
                    });
                    err.present();
                });
            }
        });
        //Validar cuando no este disponible la conexion del dispositivo
        var disconnectSubscription = this.network.onDisconnect().subscribe(function () {
            _this.vInternet = false;
            var suss = _this.toastCtrl.create({
                message: 'Sin conexion',
                duration: 3000,
                position: 'bottom'
            });
            suss.present();
        });
        //Validar cuando este disponible la conexion del dispositivo
        var connectSubscription = this.network.onConnect().subscribe(function () {
            _this.vInternet = true;
            setTimeout(function () {
                var suss = _this.toastCtrl.create({
                    message: 'Conexion activada',
                    duration: 3000,
                    position: 'bottom'
                });
                suss.present();
            }, 3000);
            /*setTimeout(() => {
              if (this.network.type === 'wifi') {
                console.log('we got a wifi connection, woohoo!');
              }
            }, 3000);*/
        });
        //--------------------------------------------------------
        this.rForm = fb.group({
            'correo': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'pass': [null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            'validate': ''
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
    };
    LoginPage.prototype.pushPage = function (params) {
        var _this = this;
        this.correo = params.correo;
        this.pass = params.correo;
        console.log(this.correo + ' ' + this.pass);
        var loader = this.loadingCtrl.create({
            content: "Validando...",
            duration: 3000
        });
        loader.present();
        setTimeout(function () {
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
        }, 3000);
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n<br><br>\n  <ion-grid>\n    <ion-row>\n\n        <ion-title text-center><strong>Sistema de Reclutamiento</strong></ion-title>\n\n    </ion-row>\n\n<!-- Mensa de login sin conexion -->\n    <ion-row justify-content-center *ngIf="vInternet == false">\n      <br><br><br><br>\n      <ion-title text-center><strong>:( Sin conexion no puedes autenticarte..</strong></ion-title>\n    </ion-row>\n\n<!-- Contenido Principal -->\n\n    <div *ngIf="vInternet == true">\n    <ion-row justify-content-center>\n\n      <ion-list>\n<br>\n\n<form [formGroup]="rForm" (ngSubmit)="pushPage(rForm.value);" novalidate>\n\n  <ion-item>\n    <ion-label stacked>Correo</ion-label>\n    <ion-input type="text" formControlName="correo" required></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label stacked>Contraseña</ion-label>\n    <ion-input type="password" formControlName="pass" required></ion-input>\n  </ion-item>\n\n  <p *ngIf="!rForm.controls[\'correo\'].valid && rForm.controls[\'correo\'].touched">Correo Requerido</p>\n  <p *ngIf="!rForm.controls[\'pass\'].valid && rForm.controls[\'pass\'].touched">Contraseña Requerida</p>\n\n<br>\n\n<ion-row justify-content-center>\n<button ion-button type="submit" [disabled]="!rForm.valid">Entrar</button>\n</ion-row>\n\n</form>\n\n</ion-list>\n\n    </ion-row>\n\n    <ion-row justify-content-center>\n      <img src="assets/img/logo.png" height="80" width="220">\n    </ion-row>\n</div>\n\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/jesusyuzetrodriguezali/proyecto_falcon/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_location_accuracy__["a" /* LocationAccuracy */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_network__["a" /* Network */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

},[213]);
//# sourceMappingURL=main.js.map