import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public image: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }


  changeImgProfile(){
    let options: CameraOptions = {
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100,
      allowEdit: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 1,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then(imageData => {
      this.image = "data:image/jpeg;base64," + imageData;
      let galleryImageSelector = document.getElementById('avatar');
       galleryImageSelector.setAttribute('src', this.image);
      /*let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: this.image,
      buttons: ['OK']});
      alert.present();*/
    }).catch(error =>{
      console.log(error);
    });
  }


}
