import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the CdDocumentosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cd-documentos',
  templateUrl: 'cd-documentos.html',
})
export class CdDocumentosPage {

  public show1:boolean = false;
  public image: string ;
  public show2:boolean = false;
  public image2: string ;
  public show3:boolean = false;
  public image3: string ;

  public rForm: FormGroup;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams,  public alertCtrl: AlertController, public loadingCtrl: LoadingController, private camera: Camera, private fb: FormBuilder) {
    this.rForm = fb.group({
      'nombre' : [null, Validators.required],
      'apellidos' : [null, Validators.required],
      'correo': [null, Validators.required],
      'fecha': [null, Validators.required],
      'celular': [null, Validators.required],
      'validate' : ''
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CdDocumentosPage');
  }

  public getPicture(){
    let options: CameraOptions = {
      targetWidth: 800,
      targetHeight: 800,
      quality: 100,
      allowEdit: true,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then(imageData => {
      this.show1=true;
      this.image = "data:image/jpeg;base64," + imageData;
      let galleryImageSelector = document.getElementById('acta');
       galleryImageSelector.setAttribute('src', this.image);
      /*let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: this.image,
      buttons: ['OK']});
      alert.present();*/
    }).catch(error =>{
      let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: error,
      buttons: ['OK']});
      alert.present();
    });
  }

  public getPicture2(){
    let options2: CameraOptions = {
      targetWidth: 800,
      targetHeight: 800,
      quality: 100,
      allowEdit: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options2).then(imageData2 => {
      this.show2=true;
      this.image2 = "data:image/jpeg;base64," + imageData2;
      let galleryImageSelector2 = document.getElementById('domicilio');
       galleryImageSelector2.setAttribute('src', this.image2);
      /*let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: this.image2,
      buttons: ['OK']});
      alert.present();*/
    }).catch(error =>{
      let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: error,
      buttons: ['OK']});
      alert.present();
    });
  }

  public getPicture3(){
    let options3: CameraOptions = {
      targetWidth: 800,
      targetHeight: 800,
      quality: 100,
      allowEdit: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options3).then(imageData3 => {
      this.show3=true;
      this.image3 = "data:image/jpeg;base64," + imageData3;
      let galleryImageSelector3 = document.getElementById('certificado');
       galleryImageSelector3.setAttribute('src', this.image3);
      /*let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: this.image,
      buttons: ['OK']});
      alert.present();*/
    }).catch(error =>{
      let alert = this.alertCtrl.create({
        title: 'Fallo',
      subTitle: error,
      buttons: ['OK']});
      alert.present();
    });
  }

  public insert(params){

    if (this.image == null){
      let modal1 = this.alertCtrl.create({
      title: 'Faltan Archivos!',
      subTitle: 'Tomar foto del documento acta!',
      buttons: ['OK']
    });
    modal1.present();
    }else if(this.image2 == null){
      let modal2 = this.alertCtrl.create({
      title: 'Faltan Archivos!',
      subTitle: 'Tomar foto del documento domicilio!',
      buttons: ['OK']
    });
    modal2.present();
    }else if(this.image3 == null){
      let modal3 = this.alertCtrl.create({
      title: 'Faltan Archivos!',
      subTitle: 'Tomar foto del documento certificado!',
      buttons: ['OK']
    });
    modal3.present();
    }else{
      let loader = this.loadingCtrl.create({
      content: "Validando Datos...",
      duration: 3000
    });
    loader.present();
    setTimeout(() => {
      let alert = this.alertCtrl.create({
      title: 'Registro exitoso!',
      subTitle: 'Se han guardado los datos del reclutado con exito!',
      buttons: ['OK']
    });
    alert.present(this.viewCtrl.dismiss());
  },3000);
  }
}

}
