import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CdDocumentosPage } from './cd-documentos';

@NgModule({
  declarations: [
    CdDocumentosPage,
  ],
  imports: [
    IonicPageModule.forChild(CdDocumentosPage),
  ],
})
export class CdDocumentosPageModule {}
