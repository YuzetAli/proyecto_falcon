import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PendingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending',
  templateUrl: 'pending.html',
})
export class PendingPage {
  public obj;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.obj=this.navParams.get("obj");
    console.log(this.obj);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingPage');
  }

  cancelModal(){
  this.viewCtrl.dismiss();
}

}
