import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { DemoService } from '../../app/services/demo.service';
import { TaskService } from '../../app/services/task-service';
import { TabsPage } from '../tabs/tabs'
@IonicPage()

@Component({
  selector: 'page-cd-basicos',
  templateUrl: 'cd-basicos.html',
  providers: [DemoService]
})
export class CdBasicosPage {

  rForm: FormGroup;
  nombre:string;
  apellidos:string;
  correo:string;
  fnacimiento:string;
  celular:string;
  params:any;
  private position:any;
  private vInternet:boolean;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController,
    public navParams: NavParams, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, private fb: FormBuilder,
    private geolocation: Geolocation, private nativeGeocoder: NativeGeocoder,
    private toastCtrl: ToastController, private network: Network,
    private locationAccuracy: LocationAccuracy, private demoService: DemoService,public tasksService: TaskService ) {

    this.rForm = fb.group({
      'nombre' : [null, Validators.required],
      'apellidos' : [null, Validators.required],
      'correo': [null, Validators.required],
      'fnacimiento': [null, Validators.required],
      'celular': [null, Validators.required],
      'validate' : ''
    });

  /*  this.geolocation.getCurrentPosition().then((resp) => {
 // resp.coords.latitude
 // resp.coords.longitude
 console.log(resp.coords.latitude + ' ' + resp.coords.longitude);

 this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
 .then((result: NativeGeocoderReverseResult) => {

   this.position = JSON.stringify(result);

   let alert12 = this.alertCtrl.create({
   title: 'Usted esta en:',
   subTitle: this.position,
   buttons: ['OK']
 });

 alert12.present();
   console.log(JSON.stringify(result));

 }).catch((error: any) => {

   let alert11 = this.alertCtrl.create({
   title: 'Usted esta en:',
   subTitle: error,
   buttons: ['OK']
   });

   alert11.present();
 });

}).catch((error) => {
  console.log('Error getting location', error);
});*/

/*Validar cuando no este disponible la conexion del dispositivo
let disconnectSubscription = this.network.onDisconnect().subscribe(() => {

  this.vInternet=false;

});
//Validar cuando este disponible la conexion del dispositivo
let connectSubscription = this.network.onConnect().subscribe(() => {

setTimeout(() => {

  this.vInternet=true;

}, 3000);

});*/

}


//---------------------------------------
  dismiss() {
    this.viewCtrl.dismiss();
  }
//---------------------------------------
  ionViewDidLoad() {
    console.log('ionViewDidLoad CdBasicosPage');
  }
//---------------------------------------
  insert(params){

    /*this.locationAccuracy.canRequest().then((canRequest: boolean) => {

  if(canRequest) {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {*/
      let loader = this.loadingCtrl.create({
          content: "Validando Datos..."
        });

        loader.present();

        this.demoService.prueba().subscribe(
          response => {

            let alertss= this.alertCtrl.create({
              title: 'Cosumo existoso',
              subTitle: 'Se han insertado los datos correctamente',
              buttons: ['OK']
            });

              loader.dismiss();

              alertss.present();
              
              this.navCtrl.setRoot(TabsPage);

          },
          error => {
            let alerterr= this.alertCtrl.create({
            title: 'No hay conexion',
            subTitle: 'Se esperara conexion para poder insertar el registro, registro guardado en la base de datos local',
            buttons: ['OK']
          });

          let alerterr2= this.alertCtrl.create({
          title: 'No hay conexion',
          subTitle: 'Se esperara conexion para poder insertar el registro, registro no guardado',
          buttons: ['OK']
        });

          this.tasksService.create(params)
            .then(response => {

              loader.dismiss();

              alerterr.present();

              this.navCtrl.setRoot(TabsPage);

            })
            .catch( error => {
              loader.dismiss();
              alerterr2.present();
            })


          });


        /*},
        error => {
          let alertGPS = this.alertCtrl.create({
               title: 'Error al reclutar',
               message: 'No se puede reclutar hasta que se encienda el gps de tu dispositivo',
               buttons: ['OK']
               });

       alertGPS.present();
    });
  }
});*/
    /*console.log(params);
  let loader = this.loadingCtrl.create({
      content: "Validando Datos...",
      duration: 3000
    });
    loader.present();
    setTimeout(() => {
      let alert = this.alertCtrl.create({
      title: 'Registro exitoso!',
      subTitle: 'Se han guardado los datos del reclutado con exito!',
      buttons: ['OK']
    });
    alert.present(this.viewCtrl.dismiss());
  },3000);*/

  }


}
