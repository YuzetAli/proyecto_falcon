import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CdBasicosPage } from './cd-basicos';

@NgModule({
  declarations: [
    CdBasicosPage,
  ],
  imports: [
    IonicPageModule.forChild(CdBasicosPage),
  ],
})
export class CdBasicosPageModule {}
