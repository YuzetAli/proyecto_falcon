import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuOpcionesPopoverPage } from './menu-opciones-popover';

@NgModule({
  declarations: [
    MenuOpcionesPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuOpcionesPopoverPage),
  ],
})
export class MenuOpcionesPopoverPageModule {}
