import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { LoginPage } from '../login/login';
/**
 * Generated class for the MenuOpcionesPopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu-opciones-popover',
  templateUrl: 'menu-opciones-popover.html',
})
export class MenuOpcionesPopoverPage {

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
    public viewCtrl: ViewController,
  	) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad MenuOpcionesPopoverPage');
  }

  close(){
    this.viewCtrl.dismiss();
  }

  cerrarSesion(){
  	this.navCtrl.push(LoginPage);
  }

}
