import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import { SentPage } from '../sent/sent';
import { PendingPage } from '../pending/pending';
import { TaskService } from '../../app/services/task-service';

/**
 * Generated class for the HistorialPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {
  tasksSent: any[] = [];

  tasksPending: any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams, public modalCtrl:ModalController,
    public tasksService: TaskService) {
  }

  ionViewDidLoad(){
    this.getAllTasksSent();
    this.getAllTasksPending();
  }



   Enviados(obj){
     let enviadosModal = this.modalCtrl.create(SentPage, {obj});
     enviadosModal.present();
  }

  Pendientes(obj){
    let pendientesModal = this.modalCtrl.create(PendingPage, {obj});
    pendientesModal.present();
 }

 getAllTasksPending(){
   this.tasksService.getAllPending()
   .then(tasks => {
     console.log(tasks);
     this.tasksPending = tasks;
   })
   .catch( error => {
     console.error( error );
   })
 }

 getAllTasksSent(){
   this.tasksService.getAllSent()
   .then(tasks => {
     console.log(tasks);
     this.tasksSent = tasks;
   })
   .catch( error => {
     console.error( error );
   })
 }

}
