import { Component } from '@angular/core';
import { NavController, ModalController, Slides, PopoverController } from 'ionic-angular';
import { CdBasicosPage } from '../cd-basicos/cd-basicos';
import { CdDocumentosPage } from '../cd-documentos/cd-documentos';
import { LoginPage } from '../login/login';
import { MenuOpcionesPopoverPage } from '../menu-opciones-popover/menu-opciones-popover';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public modalCtrl: ModalController ,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController
    ) {

  }

  basicoModal() {
    let modal = this.modalCtrl.create(CdBasicosPage);
    modal.present();
  }

  documentosModal(){
    let modal = this.modalCtrl.create(CdDocumentosPage);
    modal.present();
  }


  slideChanged() {
    //this.slides.slideNext(500, true);
    //let currentIndex = this.slides.getActiveIndex();
    //console.log('Current index is', currentIndex);
  }

  openOpciones(event){
    let popover = this.popoverCtrl.create(MenuOpcionesPopoverPage );
    popover.present({ ev: event });
    //popover.onDidDismiss();
  }
}
