import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Network } from '@ionic-native/network';
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  rForm: FormGroup;
  correo:string = '';
  pass:string = '';
  params:any;

  public vInternet = true

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, private fb: FormBuilder,
    private locationAccuracy: LocationAccuracy, private toastCtrl: ToastController,
    private network: Network) {

      this.locationAccuracy.canRequest().then((canRequest: boolean) => {

        if(canRequest) {
          // the accuracy option will be ignored by iOS
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
            () => {
              let suss = this.toastCtrl.create({
                message: 'Se ha activado la ubicacion',
                duration: 3000,
                position: 'bottom'
              });

              suss.onDidDismiss(() => {
                console.log('Dismissed toast');
              });

              suss.present();
            },
            error => {
              let err = this.toastCtrl.create({
                message: error,
                duration: 2700,
                position: 'bottom'
              });

              err.onDidDismiss(() => {
              console.log('Dismissed toast');
            });

            err.present();
            }
          );
        }
      });



      //Validar cuando no este disponible la conexion del dispositivo
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {

        this.vInternet=false;

        let suss = this.toastCtrl.create({
          message: 'Sin conexion',
          duration: 3000,
          position: 'bottom'
        });

        suss.present();

    });
//Validar cuando este disponible la conexion del dispositivo
    let connectSubscription = this.network.onConnect().subscribe(() => {

      this.vInternet=true;

      setTimeout(() => {
      let suss = this.toastCtrl.create({
        message: 'Conexion activada',
        duration: 3000,
        position: 'bottom'
      });

      suss.present();

      }, 3000);
  /*setTimeout(() => {
    if (this.network.type === 'wifi') {
      console.log('we got a wifi connection, woohoo!');
    }
  }, 3000);*/
});

    //--------------------------------------------------------

    this.rForm = fb.group({
      'correo':[null, Validators.required],
      'pass':[null, Validators.required],
      'validate': ''
    });
  }




  ionViewDidLoad() {

  }

  pushPage(params){

  this.correo = params.correo;
  this.pass = params.correo;

  console.log(this.correo + ' ' + this.pass);

  let loader = this.loadingCtrl.create({
      content: "Validando...",
      duration: 3000
    });
    loader.present();
    setTimeout(() => {
    this.navCtrl.push(TabsPage);
  },3000);

  }

}
