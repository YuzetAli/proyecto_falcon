import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MetasPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-metas',
  templateUrl: 'metas.html',
})
export class MetasPage {
  public metas = [
    {
      descripcion:"Recluta un total de 10 personas el dia de hoy",
      rangoMeta: 10,
      rangoMetaActual: 2,
    },
    {
      descripcion:"Recluta un total de 100 personas en este mes",
      rangoMeta: 100,
      rangoMetaActual: 2
    },
    {
      descripcion:"Recluta 5 personas con sus documentos",
      rangoMeta: 5,
      rangoMetaActual: 3
    },
    {
      descripcion:"Recluta un total de 4 personas",
      rangoMeta: 4,
      rangoMetaActual: 4
    },
    {
      descripcion:"Recluta 2 personas con sus documentos",
      rangoMeta: 2,
      rangoMetaActual: 2
    }
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MetasPage');
  }

}
