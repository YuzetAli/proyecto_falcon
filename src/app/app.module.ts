import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Network } from '@ionic-native/network';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { MetasPage } from '../pages/metas/metas';
import { HistorialPage } from '../pages/historial/historial';
import { CdBasicosPage } from '../pages/cd-basicos/cd-basicos';
import { CdDocumentosPage } from '../pages/cd-documentos/cd-documentos';
import { MenuOpcionesPopoverPage } from '../pages/menu-opciones-popover/menu-opciones-popover';
import { PendingPage } from '../pages/pending/pending';
import { SentPage } from '../pages/sent/sent';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { SQLite } from '@ionic-native/sqlite';
import { TaskService } from './services/task-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    ProfilePage,
    MetasPage,
    HistorialPage,
    CdBasicosPage,
    CdDocumentosPage,
    MenuOpcionesPopoverPage,
    PendingPage,
    SentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    ProfilePage,
    MetasPage,
    HistorialPage,
    CdBasicosPage,
    CdDocumentosPage,
    MenuOpcionesPopoverPage,
    PendingPage,
    SentPage
  ],
  providers: [
    StatusBar,
    Camera,
    Geolocation,
    NativeGeocoder,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GeocoderProvider,
    LocationAccuracy,
    SQLite,
    TaskService
  ]
})
export class AppModule {}
