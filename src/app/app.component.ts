import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
//import { HistorialPage } from '../pages/historial/historial';
//import { MetasPage } from '../pages/metas/metas';
import { SQLite } from '@ionic-native/sqlite';
import { TaskService } from './services/task-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public tasksService: TaskService,
    public sqlite: SQLite
  ) {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.createDatabase();
    });
  }

  private createDatabase(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default' // the location field is required
    })
    .then((db) => {
      this.tasksService.setDatabase(db);
      return this.tasksService.createTable();
    })
    .then(() =>{
      this.splashScreen.hide();
    })
    .catch(error =>{
      console.error(error);
    });
  }

}
