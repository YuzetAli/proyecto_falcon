import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLiteObject } from '@ionic-native/sqlite';
/*
  Generated class for the TaskServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TaskService {

  db: SQLiteObject = null;

  constructor(public http: Http) {
  }

  setDatabase(db: SQLiteObject){
  if(this.db === null){
    this.db = db;
  }
}

create(task: any){
  let sql = 'INSERT INTO dbasicos(nombres, apellidos, correo, fnacimiento, status) VALUES(?,?,?,?,0)';
  return this.db.executeSql(sql, [task.nombre, task.apellidos , task.correo, task.fnacimiento]);
}

createTable(){
  let sql = 'CREATE TABLE IF NOT EXISTS dbasicos(id INTEGER PRIMARY KEY AUTOINCREMENT, nombres TEXT, apellidos TEXT, correo TEXT, fnacimiento TEXT,status INTEGER)';
  return this.db.executeSql(sql, []);
}

delete(task: any){
  let sql = 'DELETE FROM dbasicos WHERE id=?';
  return this.db.executeSql(sql, [task.id]);
}

getAllPending(){
  let sql = 'SELECT * FROM dbasicos WHERE status=0';
  return this.db.executeSql(sql, [])
  .then(response => {
    let tasks = [];
    for (let index = 0; index < response.rows.length; index++) {
      tasks.push( response.rows.item(index) );
    }
    return Promise.resolve( tasks );
  })
  .catch(error => Promise.reject(error));
}

getAllSent(){
  let sql = 'SELECT * FROM dbasicos WHERE status=1';
  return this.db.executeSql(sql, [])
  .then(response => {
    let tasks = [];
    for (let index = 0; index < response.rows.length; index++) {
      tasks.push( response.rows.item(index) );
    }
    return Promise.resolve( tasks );
  })
  .catch(error => Promise.reject(error));
}

/*update(task: any){
  let sql = 'UPDATE tasks SET title=?, completed=? WHERE id=?';
  return this.db.executeSql(sql, [task.title, task.completed, task.id]);
}*/

}
